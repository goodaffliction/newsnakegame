// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPwanBase.h"


// Sets default values
APlayerPwanBase::APlayerPwanBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerPwanBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerPwanBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPwanBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

